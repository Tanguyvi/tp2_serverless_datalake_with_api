# PROVIDER

provider "aws" {
  region  = var.region
  version = "~> 3.9"
}

terraform {
  required_version = "= 0.13.3"
}